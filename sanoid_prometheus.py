#!/usr/bin/env python3

#      This file is part of sanoid_prometheus (https://gitlab.com/aaron-w/sanoid_prometheus).
#
#     sanoid_prometheus is free software: you can redistribute it and/or modify it under the terms of the GNU General
#     Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option)
#     any later version.
#
#     sanoid_prometheus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
#     implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
#     more details.
#
#     You should have received a copy of the GNU General Public License along with sanoid_prometheus. If not, see
#     <https://www.gnu.org/licenses/>.

from prometheus_client import CollectorRegistry, Gauge, write_to_textfile
import json
import subprocess

CONFIG_FILE = "/etc/sanoid/sanoid_prometheus.conf"


def registry_from_json(input_json):
    """Takes a JSON string and returns a prometheus_client CollectorRegistry"""

    registry = CollectorRegistry()


    sanoid_metrics = json.loads(input_json)

    snapshots_exist = Gauge('sanoid_snapshots_exist',
                            'Does this dataset have any Sanoid snapshots of this type (0 = No, 1 = Yes)?',
                            registry=registry, labelnames=['dataset', 'snapshot_type'])

    monitor_dont_warn = Gauge('sanoid_monitor_dont_warn',
                              'Is monitor_dont_warn set for this type (0 = No, 1 = Yes)?',
                              registry=registry, labelnames=['dataset', 'snapshot_type'])

    monitor_dont_crit = Gauge('sanoid_monitor_dont_crit',
                              'Is monitor_dont_crit set for this type (0 = No, 1 = Yes)?',
                              registry=registry, labelnames=['dataset', 'snapshot_type'])

    snapshots_warn_age_seconds = Gauge('sanoid_snapshots_warn_age_seconds',
                                       'The age in seconds that should cause a warning for this snapshot type',
                                       registry=registry, labelnames=['dataset', 'snapshot_type'])

    snapshots_critical_age_seconds = Gauge('sanoid_snapshots_critical_age_seconds',
                                           'The age in seconds that should cause a critical for this snapshot type',
                                           registry=registry, labelnames=['dataset', 'snapshot_type'])

    snapshot_newest_age_seconds = Gauge('sanoid_snapshot_newest_age_seconds',
                                        'The age of the newest snapshot of this type in seconds',
                                        registry=registry, labelnames=['dataset', 'snapshot_type'])

    snapshot_newest_ctime_seconds = Gauge('sanoid_snapshot_newest_ctime_seconds',
                                          'Unix ctime (seconds) of the newest snapshot of this type',
                                          registry=registry, labelnames=['dataset', 'snapshot_type'])

    snapshot_health_issues = Gauge('sanoid_snapshot_health_issues',
                                   'Did Sanoid report any snapshot health issues (0 = No, 1 = Warn, 2 = Critical)?',
                                   registry=registry, labelnames=['dataset', 'snapshot_type'])

    if "snapshot_info" in sanoid_metrics:
        for dataset in sanoid_metrics["snapshot_info"]:
            for snapshot_type in sanoid_metrics["snapshot_info"][dataset]:
                snapshots_exist.labels(dataset=dataset, snapshot_type=snapshot_type).set(
                    sanoid_metrics["snapshot_info"][dataset][snapshot_type]["has_snapshots"])
                monitor_dont_warn.labels(dataset=dataset, snapshot_type=snapshot_type).set(
                    sanoid_metrics["snapshot_info"][dataset][snapshot_type]["monitor_dont_warn"])
                monitor_dont_crit.labels(dataset=dataset, snapshot_type=snapshot_type).set(
                    sanoid_metrics["snapshot_info"][dataset][snapshot_type]["monitor_dont_crit"])
                snapshots_warn_age_seconds.labels(dataset=dataset, snapshot_type=snapshot_type).set(
                    sanoid_metrics["snapshot_info"][dataset][snapshot_type]["warn_age_seconds"])
                snapshots_critical_age_seconds.labels(dataset=dataset, snapshot_type=snapshot_type).set(
                    sanoid_metrics["snapshot_info"][dataset][snapshot_type]["crit_age_seconds"])

                if "newest_age_seconds" in sanoid_metrics["snapshot_info"][dataset][snapshot_type]:
                    # Will not be in there if has_snapshots != 1
                    snapshot_newest_age_seconds.labels(dataset=dataset, snapshot_type=snapshot_type).set(
                        sanoid_metrics["snapshot_info"][dataset][snapshot_type]["newest_age_seconds"])

                if "newest_snapshot_ctime_seconds" in sanoid_metrics["snapshot_info"][dataset][snapshot_type]:
                    # Added 18 Feb 2022, will not be there if there are no snapshots
                    snapshot_newest_ctime_seconds.labels(dataset=dataset, snapshot_type=snapshot_type).set(
                        sanoid_metrics["snapshot_info"][dataset][snapshot_type]["newest_snapshot_ctime_seconds"])

                if "snapshot_health_issues" in sanoid_metrics["snapshot_info"][dataset][snapshot_type]:
                    # Added 4 Feb 2022
                    snapshot_health_issues.labels(dataset=dataset, snapshot_type=snapshot_type).set(
                        sanoid_metrics["snapshot_info"][dataset][snapshot_type]["snapshot_health_issues"])
    else:
        print("No 'snapshot_info' section in the supplied JSON")

    return registry


if __name__ == '__main__':
    with open(CONFIG_FILE) as json_file:
        config = json.load(json_file)

    return_info = subprocess.run([config["sanoid_path"],  "--monitor-metrics-json"], check=True, capture_output=True)
    json_from_sanoid = return_info.stdout

    registry = registry_from_json(json_from_sanoid)
    write_to_textfile(config["prom_text_file_path"], registry)


