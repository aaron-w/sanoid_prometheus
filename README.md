## Objective of sanoid_prometheus
Sanoid (https://github.com/jimsalterjrs/sanoid/) is a ZFS auto-snapshot and synchronisation tool. It includes a number of monitoring capabilities for Nagios. This project aims to enable using Prometheus (particularly the Alertmanager functionality) with Sanoid.

Importantly, this project aims to mirror the monitoring in Sanoid. If you notice an error that is also present in the equivalent functions of Sanoid (e.g. `sanoid --monitor-snapshots`), please report the problem there instead of here.

## Dependencies
Requires `python3` and `python3-prometheus-client` (Ubuntu names, use your distribution's equivalents). 

## Getting started
The initial target for this project is to write the metrics to a .prom [textformat](https://prometheus.io/docs/instrumenting/exposition_formats/#text-format-example) file that can be scraped by the textfile collector in the [node-exporter built into Grafana Agent](https://grafana.com/docs/agent/latest/configuration/integrations/node-exporter-config/), but it should work for any Prometheus node-exporter that supports textfiles. In the future it may be extended to support exporting metrics in other formats.

### Set up Prometheus/Alertmanager, node-exporter and textfile collector
If you are new to Prometheus, you could take the following approach to start using this quickly:
1. Sign up for a [free Grafana Cloud account](https://grafana.com/auth/sign-up/create-user).
2. On your dashboard (username.grafana.net) there is an Integration for "Linux Server". Click this and follow the instructions to install the Grafana Agent on your machine.
3. Create a folder for the .prom textfile metric files, e.g. `sudo mkdir /etc/grafana-text-file-collectors/`.
4. Modify `/etc/grafana-agent.yaml` to add in the `textfile_directory` as the directory you just created.
```
integrations:
  node_exporter:
    enabled: true
    textfile_directory: /etc/grafana-text-file-collectors/
```

### Install a version of sanoid with JSON export functionality
This program requires the `--monitor-metrics-json` functionality of Sanoid, which has not yet even been merged into upstream Sanoid. You can find it in [this branch](https://github.com/Hooloovoo/sanoid/tree/add_metrics_json).

Or you can simply:
```
wget https://github.com/Hooloovoo/sanoid/raw/add_metrics_json/sanoid
```
In order to avoid conflicting with your system version of Sanoid or using this unreleased version to do your snapshotting etc, I would recommend renaming this and only using it for this monitoring:
```
sudo mv sanoid /usr/bin/sanoid_json
sudo chmod +x /usr/bin/sanoid_json
```
Note you may also need `libjson-perl` installed and, depending on the version you normally use, you may also need to put a copy of `https://github.com/jimsalterjrs/sanoid/blob/master/sanoid.defaults.conf` in your `/etc/sanoid/`.

Run `sudo /usr/bin/sanoid_json --monitor-metrics-json` manually first to check everything is working correctly -- if it is then you should see JSON output.

### Set up sanoid_prometheus
The setup for sanoid_prometheus is straightforward once the above is in place: 
1. Ensure you have the required dependencies set out above.
2. Download sanoid_prometheus to the machine to be monitored (just the `sanoid_prometheus.py` file alone is currently sufficient).
3. Copy the example `sanoid_prometheus.conf` file to `/etc/sanoid/sanoid_prometheus.conf`
4. Update `/etc/sanoid/sanoid_prometheus.conf`. `sanoid_path` is the path to the sanoid script, so if you are using the above it will be `/usr/bin/sanoid_json` and `prom_text_file_path` is where you would like the textfile to be exported, so in the above example it would be something like `/etc/grafana-text-file-collectors/sanoid.prom`.
5. You likely want to set up a cron job to run this on a regular basis. How regular will depend on how often you have node-exporter send back metrics and how granular you need your snapshot reporting to be. Adding the below to your crontab would run the command at 10 minutes past every hour (obviously updating the path to wherever you put it):
```
10 * * * * /usr/bin/sanoid_prometheus.py
```

## Example Prometheus Alertmanager rules

The below rules aim to cover all situations where sanoid reports that things were not `OK`. 

### Catch-all: Sanoid is giving a snapshot warning
The list will be updated with specific alerts that give more information, but until that is proven comprehensive, a catch-all rule that warns if there are any snapshot issues is helpful (in many cases this will fire together with another alert):
```
alert: Sanoid Snapshot Warning
expr: sanoid_snapshot_health_issues != 0
labels:
  severity: warning
annotations:
  description: Snapshot {{ $labels.device }} {{ $labels.dataset }} {{ $labels.snapshot_type }} has a warning (value is {{ $value }})
  summary: Sanoid snapshot warning
```

### Snapshots older than warn_age_seconds
Note that the ctime of the snapshot is deliberately used here in preference to `sanoid_snapshot_newest_age_seconds`. This is because `sanoid_snapshot_newest_age_seconds` is what was reported by Sanoid last time the script ran, exported the metrics, was scraped and uploaded. If something is not updating correctly, we want this warning to fire.   
```
alert: Sanoid Snapshots out of date
expr: (((time() - sanoid_snapshot_newest_ctime_seconds) > sanoid_snapshots_warn_age_seconds) and sanoid_snapshots_warn_age_seconds != 0)
labels:
  severity: warning
annotations:
  description: The newest {{ $labels.snapshot_type }} snapshot of {{ $labels.dataset }} on {{ $labels.device }} was created {{ $value | humanizeDuration }} ago, which exceeds sanoid_snapshots_warn_age_seconds.
```
