#!/usr/bin/env python3

#      This file is part of sanoid_prometheus (https://gitlab.com/aaron-w/sanoid_prometheus).
#
#     sanoid_prometheus is free software: you can redistribute it and/or modify it under the terms of the GNU General
#     Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option)
#     any later version.
#
#     sanoid_prometheus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
#     implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
#     more details.
#
#     You should have received a copy of the GNU General Public License along with sanoid_prometheus. If not, see
#     <https://www.gnu.org/licenses/>.

import unittest
import sanoid_prometheus


class TestTextExporter(unittest.TestCase):
    def test_no_snapshots(self):
        """Test where the JSON has no snapshots"""

        test_json_no_snapshot = '{"snapshot_info":{' \
                                '"test_pool_sanoid/dataset2":{' \
                                '"monthly":{' \
                                '"crit_age_seconds":3456000,"monitor_dont_warn":"0",' \
                                '"warn_age_seconds":2764800,"has_snapshots":0,"monitor_dont_crit":"0"},' \
                                '"daily":{' \
                                '"has_snapshots":0,"monitor_dont_crit":"0","crit_age_seconds":115200,' \
                                '"monitor_dont_warn":"0","warn_age_seconds":100800},' \
                                '"hourly":{"monitor_dont_crit":"0","has_snapshots":0,"warn_age_seconds":5400,' \
                                '"crit_age_seconds":21600,"monitor_dont_warn":"0"}},' \
                                '"test_pool_sanoid/dataset1":{' \
                                '"monthly":{' \
                                '"monitor_dont_crit":"0","has_snapshots":0,"warn_age_seconds":2764800,"monitor_dont_warn":"0",' \
                                '"crit_age_seconds":3456000},"daily":{"warn_age_seconds":100800,"monitor_dont_warn":"0",' \
                                '"crit_age_seconds":115200,"monitor_dont_crit":"0","has_snapshots":0},' \
                                '"hourly":{' \
                                '"warn_age_seconds":5400,"crit_age_seconds":21600,"monitor_dont_warn":"0",' \
                                '"monitor_dont_crit":"0","has_snapshots":0}},' \
                                '"test_pool_sanoid":{' \
                                '"daily":{' \
                                '"warn_age_seconds":100800,"monitor_dont_warn":"0","crit_age_seconds":115200,' \
                                '"monitor_dont_crit":"0","has_snapshots":0},' \
                                '"hourly":{' \
                                '"crit_age_seconds":21600,"monitor_dont_warn":"0","warn_age_seconds":5400,"has_snapshots":0,' \
                                '"monitor_dont_crit":"0"},' \
                                '"monthly":{' \
                                '"has_snapshots":0,"monitor_dont_crit":"0","crit_age_seconds":3456000,' \
                                '"monitor_dont_warn":"0","warn_age_seconds":2764800}},' \
                                '"test_pool_sanoid/dataset3":{' \
                                '"monthly":{' \
                                '"warn_age_seconds":2764800,"crit_age_seconds":3456000,"monitor_dont_warn":"0",' \
                                '"monitor_dont_crit":"0","has_snapshots":0},' \
                                '"hourly":{' \
                                '"warn_age_seconds":5400,"monitor_dont_warn":"0","crit_age_seconds":21600,' \
                                '"monitor_dont_crit":"0","has_snapshots":0},' \
                                '"daily":{' \
                                '"warn_age_seconds":100800,"crit_age_seconds":115200,"monitor_dont_warn":"0",' \
                                '"monitor_dont_crit":"0","has_snapshots":0}}},' \
                                '"schema_version":202201301}'

        registry = sanoid_prometheus.registry_from_json(test_json_no_snapshot)

        # Test sanoid_snapshots_exist
        self.assertEqual(registry.get_sample_value(
            "sanoid_snapshots_exist", {"dataset": "test_pool_sanoid", "snapshot_type": "hourly"}), 0.0)
        self.assertEqual(registry.get_sample_value(
            "sanoid_snapshots_exist", {"dataset": "test_pool_sanoid", "snapshot_type": "daily"}), 0.0)
        self.assertEqual(registry.get_sample_value(
            "sanoid_snapshots_exist", {"dataset": "test_pool_sanoid", "snapshot_type": "monthly"}), 0.0)

        self.assertEqual(registry.get_sample_value(
            "sanoid_snapshots_exist", {"dataset": "test_pool_sanoid/dataset1", "snapshot_type": "hourly"}), 0.0)
        self.assertEqual(registry.get_sample_value(
            "sanoid_snapshots_exist", {"dataset": "test_pool_sanoid/dataset1", "snapshot_type": "daily"}), 0.0)
        self.assertEqual(registry.get_sample_value(
            "sanoid_snapshots_exist", {"dataset": "test_pool_sanoid/dataset1", "snapshot_type": "monthly"}), 0.0)

        self.assertEqual(registry.get_sample_value(
            "sanoid_snapshots_exist", {"dataset": "test_pool_sanoid/dataset2", "snapshot_type": "hourly"}), 0.0)
        self.assertEqual(registry.get_sample_value(
            "sanoid_snapshots_exist", {"dataset": "test_pool_sanoid/dataset2", "snapshot_type": "daily"}), 0.0)
        self.assertEqual(registry.get_sample_value(
            "sanoid_snapshots_exist", {"dataset": "test_pool_sanoid/dataset2", "snapshot_type": "monthly"}), 0.0)

        self.assertEqual(registry.get_sample_value(
            "sanoid_snapshots_exist", {"dataset": "test_pool_sanoid/dataset3", "snapshot_type": "hourly"}), 0.0)
        self.assertEqual(registry.get_sample_value(
            "sanoid_snapshots_exist", {"dataset": "test_pool_sanoid/dataset3", "snapshot_type": "daily"}), 0.0)
        self.assertEqual(registry.get_sample_value(
            "sanoid_snapshots_exist", {"dataset": "test_pool_sanoid/dataset3", "snapshot_type": "monthly"}), 0.0)

        # Test sanoid_monitor_dont_warn
        self.assertEqual(registry.get_sample_value(
            "sanoid_monitor_dont_warn", {"dataset": "test_pool_sanoid", "snapshot_type": "hourly"}), 0.0)
        self.assertEqual(registry.get_sample_value(
            "sanoid_monitor_dont_warn", {"dataset": "test_pool_sanoid", "snapshot_type": "daily"}), 0.0)
        self.assertEqual(registry.get_sample_value(
            "sanoid_monitor_dont_warn", {"dataset": "test_pool_sanoid", "snapshot_type": "monthly"}), 0.0)

        self.assertEqual(registry.get_sample_value(
            "sanoid_monitor_dont_warn", {"dataset": "test_pool_sanoid/dataset1", "snapshot_type": "hourly"}), 0.0)
        self.assertEqual(registry.get_sample_value(
            "sanoid_monitor_dont_warn", {"dataset": "test_pool_sanoid/dataset1", "snapshot_type": "daily"}), 0.0)
        self.assertEqual(registry.get_sample_value(
            "sanoid_monitor_dont_warn", {"dataset": "test_pool_sanoid/dataset1", "snapshot_type": "monthly"}), 0.0)

        self.assertEqual(registry.get_sample_value(
            "sanoid_monitor_dont_warn", {"dataset": "test_pool_sanoid/dataset2", "snapshot_type": "hourly"}), 0.0)
        self.assertEqual(registry.get_sample_value(
            "sanoid_monitor_dont_warn", {"dataset": "test_pool_sanoid/dataset2", "snapshot_type": "daily"}), 0.0)
        self.assertEqual(registry.get_sample_value(
            "sanoid_monitor_dont_warn", {"dataset": "test_pool_sanoid/dataset2", "snapshot_type": "monthly"}), 0.0)

        self.assertEqual(registry.get_sample_value(
            "sanoid_monitor_dont_warn", {"dataset": "test_pool_sanoid/dataset3", "snapshot_type": "hourly"}), 0.0)
        self.assertEqual(registry.get_sample_value(
            "sanoid_monitor_dont_warn", {"dataset": "test_pool_sanoid/dataset3", "snapshot_type": "daily"}), 0.0)
        self.assertEqual(registry.get_sample_value(
            "sanoid_monitor_dont_warn", {"dataset": "test_pool_sanoid/dataset3", "snapshot_type": "monthly"}), 0.0)

        # Test sanoid_monitor_dont_crit
        self.assertEqual(registry.get_sample_value(
            "sanoid_monitor_dont_crit", {"dataset": "test_pool_sanoid", "snapshot_type": "hourly"}), 0.0)
        self.assertEqual(registry.get_sample_value(
            "sanoid_monitor_dont_crit", {"dataset": "test_pool_sanoid", "snapshot_type": "daily"}), 0.0)
        self.assertEqual(registry.get_sample_value(
            "sanoid_monitor_dont_crit", {"dataset": "test_pool_sanoid", "snapshot_type": "monthly"}), 0.0)

        self.assertEqual(registry.get_sample_value(
            "sanoid_monitor_dont_crit", {"dataset": "test_pool_sanoid/dataset1", "snapshot_type": "hourly"}), 0.0)
        self.assertEqual(registry.get_sample_value(
            "sanoid_monitor_dont_crit", {"dataset": "test_pool_sanoid/dataset1", "snapshot_type": "daily"}), 0.0)
        self.assertEqual(registry.get_sample_value(
            "sanoid_monitor_dont_crit", {"dataset": "test_pool_sanoid/dataset1", "snapshot_type": "monthly"}), 0.0)

        self.assertEqual(registry.get_sample_value(
            "sanoid_monitor_dont_crit", {"dataset": "test_pool_sanoid/dataset2", "snapshot_type": "hourly"}), 0.0)
        self.assertEqual(registry.get_sample_value(
            "sanoid_monitor_dont_crit", {"dataset": "test_pool_sanoid/dataset2", "snapshot_type": "daily"}), 0.0)
        self.assertEqual(registry.get_sample_value(
            "sanoid_monitor_dont_crit", {"dataset": "test_pool_sanoid/dataset2", "snapshot_type": "monthly"}), 0.0)

        self.assertEqual(registry.get_sample_value(
            "sanoid_monitor_dont_crit", {"dataset": "test_pool_sanoid/dataset3", "snapshot_type": "hourly"}), 0.0)
        self.assertEqual(registry.get_sample_value(
            "sanoid_monitor_dont_crit", {"dataset": "test_pool_sanoid/dataset3", "snapshot_type": "daily"}), 0.0)
        self.assertEqual(registry.get_sample_value(
            "sanoid_monitor_dont_crit", {"dataset": "test_pool_sanoid/dataset3", "snapshot_type": "monthly"}), 0.0)

        # Test sanoid_snapshots_warn_age_seconds
        self.assertEqual(registry.get_sample_value(
            "sanoid_snapshots_warn_age_seconds", {"dataset": "test_pool_sanoid", "snapshot_type": "hourly"}),
            5400.0)
        self.assertEqual(registry.get_sample_value(
            "sanoid_snapshots_warn_age_seconds", {"dataset": "test_pool_sanoid", "snapshot_type": "daily"}),
            100800.0)
        self.assertEqual(registry.get_sample_value(
            "sanoid_snapshots_warn_age_seconds", {"dataset": "test_pool_sanoid", "snapshot_type": "monthly"}),
            2.7648e+06)

        self.assertEqual(registry.get_sample_value(
            "sanoid_snapshots_warn_age_seconds", {"dataset": "test_pool_sanoid/dataset1", "snapshot_type": "hourly"}),
            5400.0)
        self.assertEqual(registry.get_sample_value(
            "sanoid_snapshots_warn_age_seconds", {"dataset": "test_pool_sanoid/dataset1", "snapshot_type": "daily"}),
            100800.0)
        self.assertEqual(registry.get_sample_value(
            "sanoid_snapshots_warn_age_seconds", {"dataset": "test_pool_sanoid/dataset1", "snapshot_type": "monthly"}),
            2.7648e+06)

        self.assertEqual(registry.get_sample_value(
            "sanoid_snapshots_warn_age_seconds", {"dataset": "test_pool_sanoid/dataset2", "snapshot_type": "hourly"}),
            5400.0)
        self.assertEqual(registry.get_sample_value(
            "sanoid_snapshots_warn_age_seconds", {"dataset": "test_pool_sanoid/dataset2", "snapshot_type": "daily"}),
            100800.0)
        self.assertEqual(registry.get_sample_value(
            "sanoid_snapshots_warn_age_seconds", {"dataset": "test_pool_sanoid/dataset2", "snapshot_type": "monthly"}),
            2.7648e+06)

        self.assertEqual(registry.get_sample_value(
            "sanoid_snapshots_warn_age_seconds", {"dataset": "test_pool_sanoid/dataset3", "snapshot_type": "hourly"}),
            5400.0)
        self.assertEqual(registry.get_sample_value(
            "sanoid_snapshots_warn_age_seconds", {"dataset": "test_pool_sanoid/dataset3", "snapshot_type": "daily"}),
            100800.0)
        self.assertEqual(registry.get_sample_value(
            "sanoid_snapshots_warn_age_seconds", {"dataset": "test_pool_sanoid/dataset3", "snapshot_type": "monthly"}),
            2.7648e+06)

        # Test sanoid_snapshots_critical_age_seconds
        self.assertEqual(registry.get_sample_value(
            "sanoid_snapshots_critical_age_seconds", {"dataset": "test_pool_sanoid", "snapshot_type": "hourly"}),
            21600.0)
        self.assertEqual(registry.get_sample_value(
            "sanoid_snapshots_critical_age_seconds", {"dataset": "test_pool_sanoid", "snapshot_type": "daily"}),
            115200.0)
        self.assertEqual(registry.get_sample_value(
            "sanoid_snapshots_critical_age_seconds", {"dataset": "test_pool_sanoid", "snapshot_type": "monthly"}),
            3.456e+06)

        self.assertEqual(registry.get_sample_value(
            "sanoid_snapshots_critical_age_seconds",
            {"dataset": "test_pool_sanoid/dataset1", "snapshot_type": "hourly"}), 21600.0)
        self.assertEqual(registry.get_sample_value(
            "sanoid_snapshots_critical_age_seconds",
            {"dataset": "test_pool_sanoid/dataset1", "snapshot_type": "daily"}), 115200.0)
        self.assertEqual(registry.get_sample_value(
            "sanoid_snapshots_critical_age_seconds",
            {"dataset": "test_pool_sanoid/dataset1", "snapshot_type": "monthly"}), 3.456e+06)

        self.assertEqual(registry.get_sample_value(
            "sanoid_snapshots_critical_age_seconds",
            {"dataset": "test_pool_sanoid/dataset2", "snapshot_type": "hourly"}), 21600.0)
        self.assertEqual(registry.get_sample_value(
            "sanoid_snapshots_critical_age_seconds",
            {"dataset": "test_pool_sanoid/dataset2", "snapshot_type": "daily"}), 115200.0)
        self.assertEqual(registry.get_sample_value(
            "sanoid_snapshots_critical_age_seconds",
            {"dataset": "test_pool_sanoid/dataset2", "snapshot_type": "monthly"}), 3.456e+06)

        self.assertEqual(registry.get_sample_value(
            "sanoid_snapshots_critical_age_seconds",
            {"dataset": "test_pool_sanoid/dataset3", "snapshot_type": "hourly"}), 21600.0)
        self.assertEqual(registry.get_sample_value(
            "sanoid_snapshots_critical_age_seconds",
            {"dataset": "test_pool_sanoid/dataset3", "snapshot_type": "daily"}), 115200.0)
        self.assertEqual(registry.get_sample_value(
            "sanoid_snapshots_critical_age_seconds",
            {"dataset": "test_pool_sanoid/dataset3", "snapshot_type": "monthly"}), 3.456e+06)

    def test_with_recent_snapshots(self):
        """Test where the JSON has recent snapshots"""

        test_json_no_snapshot = '{"snapshot_info":{"test_pool_sanoid/dataset2":{"hourly":{"newest_age_seconds":218,' \
                                '"monitor_dont_warn":"0","has_snapshots":1,"monitor_dont_crit":"0",' \
                                '"warn_age_seconds":5400,"crit_age_seconds":21600},"monthly":{' \
                                '"monitor_dont_warn":"0","newest_age_seconds":218,"has_snapshots":1,' \
                                '"crit_age_seconds":3456000,"warn_age_seconds":2764800,"monitor_dont_crit":"0"},' \
                                '"daily":{"crit_age_seconds":115200,"monitor_dont_crit":"0",' \
                                '"warn_age_seconds":100800,"has_snapshots":1,"monitor_dont_warn":"0",' \
                                '"newest_age_seconds":218}},"test_pool_sanoid":{"monthly":{' \
                                '"crit_age_seconds":3456000,"warn_age_seconds":2764800,"monitor_dont_crit":"0",' \
                                '"has_snapshots":1,"monitor_dont_warn":"0","newest_age_seconds":218},"hourly":{' \
                                '"has_snapshots":1,"monitor_dont_crit":"0","warn_age_seconds":5400,' \
                                '"crit_age_seconds":21600,"newest_age_seconds":218,"monitor_dont_warn":"0"},' \
                                '"daily":{"has_snapshots":1,"monitor_dont_crit":"0","warn_age_seconds":100800,' \
                                '"crit_age_seconds":115200,"newest_age_seconds":218,"monitor_dont_warn":"0"}},' \
                                '"test_pool_sanoid/dataset3":{"hourly":{"crit_age_seconds":21600,' \
                                '"monitor_dont_crit":"0","warn_age_seconds":5400,"has_snapshots":1,' \
                                '"monitor_dont_warn":"0","newest_age_seconds":218},"monthly":{' \
                                '"newest_age_seconds":218,"monitor_dont_warn":"0","has_snapshots":1,' \
                                '"monitor_dont_crit":"0","warn_age_seconds":2764800,"crit_age_seconds":3456000},' \
                                '"daily":{"newest_age_seconds":218,"monitor_dont_warn":"0","has_snapshots":1,' \
                                '"monitor_dont_crit":"0","warn_age_seconds":100800,"crit_age_seconds":115200}},' \
                                '"test_pool_sanoid/dataset1":{"monthly":{"monitor_dont_warn":"0",' \
                                '"newest_age_seconds":217,"has_snapshots":1,"crit_age_seconds":3456000,' \
                                '"monitor_dont_crit":"0","warn_age_seconds":2764800},"hourly":{' \
                                '"crit_age_seconds":21600,"monitor_dont_crit":"0","warn_age_seconds":5400,' \
                                '"has_snapshots":1,"monitor_dont_warn":"0","newest_age_seconds":217},"daily":{' \
                                '"newest_age_seconds":217,"monitor_dont_warn":"0","warn_age_seconds":100800,' \
                                '"monitor_dont_crit":"0","crit_age_seconds":115200,"has_snapshots":1}}},' \
                                '"schema_version":202201301}'

        registry = sanoid_prometheus.registry_from_json(test_json_no_snapshot)

        # Test sanoid_snapshots_exist
        self.assertEqual(registry.get_sample_value(
            "sanoid_snapshots_exist", {"dataset": "test_pool_sanoid", "snapshot_type": "hourly"}), 1.0)
        self.assertEqual(registry.get_sample_value(
            "sanoid_snapshots_exist", {"dataset": "test_pool_sanoid", "snapshot_type": "daily"}), 1.0)
        self.assertEqual(registry.get_sample_value(
            "sanoid_snapshots_exist", {"dataset": "test_pool_sanoid", "snapshot_type": "monthly"}), 1.0)

        self.assertEqual(registry.get_sample_value(
            "sanoid_snapshots_exist", {"dataset": "test_pool_sanoid/dataset1", "snapshot_type": "hourly"}), 1.0)
        self.assertEqual(registry.get_sample_value(
            "sanoid_snapshots_exist", {"dataset": "test_pool_sanoid/dataset1", "snapshot_type": "daily"}), 1.0)
        self.assertEqual(registry.get_sample_value(
            "sanoid_snapshots_exist", {"dataset": "test_pool_sanoid/dataset1", "snapshot_type": "monthly"}), 1.0)

        self.assertEqual(registry.get_sample_value(
            "sanoid_snapshots_exist", {"dataset": "test_pool_sanoid/dataset2", "snapshot_type": "hourly"}), 1.0)
        self.assertEqual(registry.get_sample_value(
            "sanoid_snapshots_exist", {"dataset": "test_pool_sanoid/dataset2", "snapshot_type": "daily"}), 1.0)
        self.assertEqual(registry.get_sample_value(
            "sanoid_snapshots_exist", {"dataset": "test_pool_sanoid/dataset2", "snapshot_type": "monthly"}), 1.0)

        self.assertEqual(registry.get_sample_value(
            "sanoid_snapshots_exist", {"dataset": "test_pool_sanoid/dataset3", "snapshot_type": "hourly"}), 1.0)
        self.assertEqual(registry.get_sample_value(
            "sanoid_snapshots_exist", {"dataset": "test_pool_sanoid/dataset3", "snapshot_type": "daily"}), 1.0)
        self.assertEqual(registry.get_sample_value(
            "sanoid_snapshots_exist", {"dataset": "test_pool_sanoid/dataset3", "snapshot_type": "monthly"}), 1.0)

        # Test sanoid_monitor_dont_warn
        self.assertEqual(registry.get_sample_value(
            "sanoid_monitor_dont_warn", {"dataset": "test_pool_sanoid", "snapshot_type": "hourly"}), 0.0)
        self.assertEqual(registry.get_sample_value(
            "sanoid_monitor_dont_warn", {"dataset": "test_pool_sanoid", "snapshot_type": "daily"}), 0.0)
        self.assertEqual(registry.get_sample_value(
            "sanoid_monitor_dont_warn", {"dataset": "test_pool_sanoid", "snapshot_type": "monthly"}), 0.0)

        self.assertEqual(registry.get_sample_value(
            "sanoid_monitor_dont_warn", {"dataset": "test_pool_sanoid/dataset1", "snapshot_type": "hourly"}), 0.0)
        self.assertEqual(registry.get_sample_value(
            "sanoid_monitor_dont_warn", {"dataset": "test_pool_sanoid/dataset1", "snapshot_type": "daily"}), 0.0)
        self.assertEqual(registry.get_sample_value(
            "sanoid_monitor_dont_warn", {"dataset": "test_pool_sanoid/dataset1", "snapshot_type": "monthly"}), 0.0)

        self.assertEqual(registry.get_sample_value(
            "sanoid_monitor_dont_warn", {"dataset": "test_pool_sanoid/dataset2", "snapshot_type": "hourly"}), 0.0)
        self.assertEqual(registry.get_sample_value(
            "sanoid_monitor_dont_warn", {"dataset": "test_pool_sanoid/dataset2", "snapshot_type": "daily"}), 0.0)
        self.assertEqual(registry.get_sample_value(
            "sanoid_monitor_dont_warn", {"dataset": "test_pool_sanoid/dataset2", "snapshot_type": "monthly"}), 0.0)

        self.assertEqual(registry.get_sample_value(
            "sanoid_monitor_dont_warn", {"dataset": "test_pool_sanoid/dataset3", "snapshot_type": "hourly"}), 0.0)
        self.assertEqual(registry.get_sample_value(
            "sanoid_monitor_dont_warn", {"dataset": "test_pool_sanoid/dataset3", "snapshot_type": "daily"}), 0.0)
        self.assertEqual(registry.get_sample_value(
            "sanoid_monitor_dont_warn", {"dataset": "test_pool_sanoid/dataset3", "snapshot_type": "monthly"}), 0.0)

        # Test sanoid_monitor_dont_crit
        self.assertEqual(registry.get_sample_value(
            "sanoid_monitor_dont_crit", {"dataset": "test_pool_sanoid", "snapshot_type": "hourly"}), 0.0)
        self.assertEqual(registry.get_sample_value(
            "sanoid_monitor_dont_crit", {"dataset": "test_pool_sanoid", "snapshot_type": "daily"}), 0.0)
        self.assertEqual(registry.get_sample_value(
            "sanoid_monitor_dont_crit", {"dataset": "test_pool_sanoid", "snapshot_type": "monthly"}), 0.0)

        self.assertEqual(registry.get_sample_value(
            "sanoid_monitor_dont_crit", {"dataset": "test_pool_sanoid/dataset1", "snapshot_type": "hourly"}), 0.0)
        self.assertEqual(registry.get_sample_value(
            "sanoid_monitor_dont_crit", {"dataset": "test_pool_sanoid/dataset1", "snapshot_type": "daily"}), 0.0)
        self.assertEqual(registry.get_sample_value(
            "sanoid_monitor_dont_crit", {"dataset": "test_pool_sanoid/dataset1", "snapshot_type": "monthly"}), 0.0)

        self.assertEqual(registry.get_sample_value(
            "sanoid_monitor_dont_crit", {"dataset": "test_pool_sanoid/dataset2", "snapshot_type": "hourly"}), 0.0)
        self.assertEqual(registry.get_sample_value(
            "sanoid_monitor_dont_crit", {"dataset": "test_pool_sanoid/dataset2", "snapshot_type": "daily"}), 0.0)
        self.assertEqual(registry.get_sample_value(
            "sanoid_monitor_dont_crit", {"dataset": "test_pool_sanoid/dataset2", "snapshot_type": "monthly"}), 0.0)

        self.assertEqual(registry.get_sample_value(
            "sanoid_monitor_dont_crit", {"dataset": "test_pool_sanoid/dataset3", "snapshot_type": "hourly"}), 0.0)
        self.assertEqual(registry.get_sample_value(
            "sanoid_monitor_dont_crit", {"dataset": "test_pool_sanoid/dataset3", "snapshot_type": "daily"}), 0.0)
        self.assertEqual(registry.get_sample_value(
            "sanoid_monitor_dont_crit", {"dataset": "test_pool_sanoid/dataset3", "snapshot_type": "monthly"}), 0.0)

        # Test sanoid_snapshots_warn_age_seconds
        self.assertEqual(registry.get_sample_value(
            "sanoid_snapshots_warn_age_seconds", {"dataset": "test_pool_sanoid", "snapshot_type": "hourly"}),
            5400.0)
        self.assertEqual(registry.get_sample_value(
            "sanoid_snapshots_warn_age_seconds", {"dataset": "test_pool_sanoid", "snapshot_type": "daily"}),
            100800.0)
        self.assertEqual(registry.get_sample_value(
            "sanoid_snapshots_warn_age_seconds", {"dataset": "test_pool_sanoid", "snapshot_type": "monthly"}),
            2.7648e+06)

        self.assertEqual(registry.get_sample_value(
            "sanoid_snapshots_warn_age_seconds", {"dataset": "test_pool_sanoid/dataset1", "snapshot_type": "hourly"}),
            5400.0)
        self.assertEqual(registry.get_sample_value(
            "sanoid_snapshots_warn_age_seconds", {"dataset": "test_pool_sanoid/dataset1", "snapshot_type": "daily"}),
            100800.0)
        self.assertEqual(registry.get_sample_value(
            "sanoid_snapshots_warn_age_seconds", {"dataset": "test_pool_sanoid/dataset1", "snapshot_type": "monthly"}),
            2.7648e+06)

        self.assertEqual(registry.get_sample_value(
            "sanoid_snapshots_warn_age_seconds", {"dataset": "test_pool_sanoid/dataset2", "snapshot_type": "hourly"}),
            5400.0)
        self.assertEqual(registry.get_sample_value(
            "sanoid_snapshots_warn_age_seconds", {"dataset": "test_pool_sanoid/dataset2", "snapshot_type": "daily"}),
            100800.0)
        self.assertEqual(registry.get_sample_value(
            "sanoid_snapshots_warn_age_seconds", {"dataset": "test_pool_sanoid/dataset2", "snapshot_type": "monthly"}),
            2.7648e+06)

        self.assertEqual(registry.get_sample_value(
            "sanoid_snapshots_warn_age_seconds", {"dataset": "test_pool_sanoid/dataset3", "snapshot_type": "hourly"}),
            5400.0)
        self.assertEqual(registry.get_sample_value(
            "sanoid_snapshots_warn_age_seconds", {"dataset": "test_pool_sanoid/dataset3", "snapshot_type": "daily"}),
            100800.0)
        self.assertEqual(registry.get_sample_value(
            "sanoid_snapshots_warn_age_seconds", {"dataset": "test_pool_sanoid/dataset3", "snapshot_type": "monthly"}),
            2.7648e+06)

        # Test sanoid_snapshots_critical_age_seconds
        self.assertEqual(registry.get_sample_value(
            "sanoid_snapshots_critical_age_seconds", {"dataset": "test_pool_sanoid", "snapshot_type": "hourly"}),
            21600.0)
        self.assertEqual(registry.get_sample_value(
            "sanoid_snapshots_critical_age_seconds", {"dataset": "test_pool_sanoid", "snapshot_type": "daily"}),
            115200.0)
        self.assertEqual(registry.get_sample_value(
            "sanoid_snapshots_critical_age_seconds", {"dataset": "test_pool_sanoid", "snapshot_type": "monthly"}),
            3.456e+06)

        self.assertEqual(registry.get_sample_value(
            "sanoid_snapshots_critical_age_seconds",
            {"dataset": "test_pool_sanoid/dataset1", "snapshot_type": "hourly"}), 21600.0)
        self.assertEqual(registry.get_sample_value(
            "sanoid_snapshots_critical_age_seconds",
            {"dataset": "test_pool_sanoid/dataset1", "snapshot_type": "daily"}), 115200.0)
        self.assertEqual(registry.get_sample_value(
            "sanoid_snapshots_critical_age_seconds",
            {"dataset": "test_pool_sanoid/dataset1", "snapshot_type": "monthly"}), 3.456e+06)

        self.assertEqual(registry.get_sample_value(
            "sanoid_snapshots_critical_age_seconds",
            {"dataset": "test_pool_sanoid/dataset2", "snapshot_type": "hourly"}), 21600.0)
        self.assertEqual(registry.get_sample_value(
            "sanoid_snapshots_critical_age_seconds",
            {"dataset": "test_pool_sanoid/dataset2", "snapshot_type": "daily"}), 115200.0)
        self.assertEqual(registry.get_sample_value(
            "sanoid_snapshots_critical_age_seconds",
            {"dataset": "test_pool_sanoid/dataset2", "snapshot_type": "monthly"}), 3.456e+06)

        self.assertEqual(registry.get_sample_value(
            "sanoid_snapshots_critical_age_seconds",
            {"dataset": "test_pool_sanoid/dataset3", "snapshot_type": "hourly"}), 21600.0)
        self.assertEqual(registry.get_sample_value(
            "sanoid_snapshots_critical_age_seconds",
            {"dataset": "test_pool_sanoid/dataset3", "snapshot_type": "daily"}), 115200.0)
        self.assertEqual(registry.get_sample_value(
            "sanoid_snapshots_critical_age_seconds",
            {"dataset": "test_pool_sanoid/dataset3", "snapshot_type": "monthly"}), 3.456e+06)

        # Test sanoid_snapshot_newest_age_seconds
        self.assertEqual(registry.get_sample_value(
            "sanoid_snapshot_newest_age_seconds", {"dataset": "test_pool_sanoid", "snapshot_type": "hourly"}), 218.0)
        self.assertEqual(registry.get_sample_value(
            "sanoid_snapshot_newest_age_seconds", {"dataset": "test_pool_sanoid", "snapshot_type": "daily"}), 218.0)
        self.assertEqual(registry.get_sample_value(
            "sanoid_snapshot_newest_age_seconds", {"dataset": "test_pool_sanoid", "snapshot_type": "monthly"}), 218.0)

        self.assertEqual(registry.get_sample_value(
            "sanoid_snapshot_newest_age_seconds", {"dataset": "test_pool_sanoid/dataset1", "snapshot_type": "hourly"}),
            217.0)
        self.assertEqual(registry.get_sample_value(
            "sanoid_snapshot_newest_age_seconds", {"dataset": "test_pool_sanoid/dataset1", "snapshot_type": "daily"}),
            217.0)
        self.assertEqual(registry.get_sample_value(
            "sanoid_snapshot_newest_age_seconds", {"dataset": "test_pool_sanoid/dataset1", "snapshot_type": "monthly"}),
            217.0)

        self.assertEqual(registry.get_sample_value(
            "sanoid_snapshot_newest_age_seconds", {"dataset": "test_pool_sanoid/dataset2", "snapshot_type": "hourly"}),
            218.0)
        self.assertEqual(registry.get_sample_value(
            "sanoid_snapshot_newest_age_seconds", {"dataset": "test_pool_sanoid/dataset2", "snapshot_type": "daily"}),
            218.0)
        self.assertEqual(registry.get_sample_value(
            "sanoid_snapshot_newest_age_seconds", {"dataset": "test_pool_sanoid/dataset2", "snapshot_type": "monthly"}),
            218.0)

        self.assertEqual(registry.get_sample_value(
            "sanoid_snapshot_newest_age_seconds", {"dataset": "test_pool_sanoid/dataset3", "snapshot_type": "hourly"}),
            218.0)
        self.assertEqual(registry.get_sample_value(
            "sanoid_snapshot_newest_age_seconds", {"dataset": "test_pool_sanoid/dataset3", "snapshot_type": "daily"}),
            218.0)
        self.assertEqual(registry.get_sample_value(
            "sanoid_snapshot_newest_age_seconds", {"dataset": "test_pool_sanoid/dataset3", "snapshot_type": "monthly"}),
            218.0)


if __name__ == '__main__':
    unittest.main()
